# 2180608877

Name Application: Vocabulary Flash Card LDD

| User Story Title:    | Test Vocabulary                                                                                                               |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------|
| Value Statement:     | As a learner, I want to test my knowledge through vocabulary tests, so that I can view my progress.                         |
| Acceptance Criteria: | **_Acceptance Citerion :_**  <br> Given that learned start a quiz from the application  <br> When they review all vocabularied that they have learned <br> Then they can view all vocabularies that they don't remember<br> 
| Definition of Done   | All Acceptance Criteria have been met. <br> The vocabulary quiz has been tested and is error-free.<br> The vocabulary quiz has been deployed and is stable.<br> Unit Test passed                                                    <br>Code Reviewed                                                                                                                 |
| Owner                | Responsible person: Linh Dang                                                                                                 |
| Iteration            | Iteration: Sprint 2 weeks                                                                                                     |
| Estimate             | Estimate: 5 point                                                                                                              |
|                      |                                                                                                                               |


